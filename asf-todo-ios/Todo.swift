import UIKit

class Todo {
    let id : Int
    let description : String
    let inclusion : Date
    let isdone : Bool
    
    init(id: Int, description : String, inclusion: Date, isdone: Bool) {
        self.id = id
        self.description = description
        self.inclusion = inclusion
        self.isdone = isdone
    }
}
