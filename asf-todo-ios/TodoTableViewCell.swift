import UIKit

class TodoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgStatus: UIImageView!
    
    @IBOutlet weak var labelInclusion: UILabel!
    
    @IBOutlet weak var labelDescription: UILabel!

    func dateFormatToString(date : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy - HH:mm"
        return formatter.string(from: date) + "h"
    }
}
