import SQLite
import UIKit

class TodoRepository {
    
    var database: Connection!
    
    let todosTable = Table("todos")
    let id = Expression<Int>("id")
    let description = Expression<String>("description")
    let inclusion = Expression<Date>("inclusion")
    let isdone = Expression<Bool>("isdone")

    init() {

        let createTable = self.todosTable.create { (table) in
            table.column(self.id, primaryKey: true)
            table.column(self.description)
            table.column(self.inclusion)
            table.column(self.isdone)
        }
        
        do {
            let directory = try FileManager.default.url(
                for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = directory.appendingPathComponent("todos").appendingPathExtension("sqlite3")
            self.database = try Connection(fileUrl.path)
            
            let tableExists = try self.database
                .scalar("SELECT EXISTS (SELECT * FROM sqlite_master WHERE type = 'table' AND name = 'todos')") as! Int64 > 0
            
            if !tableExists {
                try self.database.run(createTable)
                print("create table")
            }
            
        } catch {
            print(error)
        }
    }
    
    func AddItem(todo: String) {
        
        do {
            let insert = todosTable.insert(description <- todo, inclusion <- Date(), isdone <- false)
            try database.run(insert)
        }
        
        catch {
            print(error)
        }
    }
    
    func removeItem(idSelected: Int) {
        
        let row = todosTable.filter(id == idSelected)

        do {
            try database.run(row.delete())
        } catch {
            print(error)
        }
    }
    
    func updateItem(idSelected: Int, isDoneTo: Bool)
    {
        let row = todosTable.filter(id == idSelected)
        
        do {
           try database.run(row.update(isdone <- isDoneTo))
        } catch {
            print(error)
        }
    }
    
    func getTodoItems(done: Bool?) -> [Todo] {
        
        var todoList:[Todo] = []

        do {
            
            var todos = todosTable
            
            if done == true || done == false {
                todos = todosTable.filter(isdone == done ?? false)
            }
            
            for todo in try database.prepare(todos) {
                todoList.append(Todo(id: todo[id], description: todo[description], inclusion: todo[inclusion], isdone: todo[isdone]))
            }
        }
        
        catch {
            print(error)
        }
        
        return todoList
    }
}
