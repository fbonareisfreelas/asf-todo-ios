import UIKit

class DoneViewController: MainViewController {
    
    @IBOutlet weak var tableViewDone: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewDone.delegate = self
        self.tableViewDone.dataSource = self
        
        let newTodoButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.newTodo))
        newTodoButton.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.topItem?.setRightBarButton(newTodoButton, animated: true)
    }
   
    override func viewWillAppear(_ animated: Bool) {
        self.todoList = self.todoRepository.getTodoItems(done: true)
        self.tableViewDone.reloadData()
        self.navigationController?.navigationBar.topItem?.title = "Feitas (" + String(todoList.count) + ")"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "ASF To Do", message: "O que deseja fazer com essa tarefa?", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Marcar como não feita", style: .default , handler:{ (UIAlertAction)in
            self.todoRepository.updateItem(idSelected: self.todoList[indexPath.row].id, isDoneTo: false)
            
            self.todoList = self.todoRepository.getTodoItems(done: true)
            self.tableViewDone.reloadData()
            self.navigationController?.navigationBar.topItem?.title = "Feitas (" + String(self.todoList.count) + ")"
        }))
        
        alert.addAction(UIAlertAction(title: "Remover", style: .destructive , handler:{ (UIAlertAction)in
            self.todoRepository.removeItem(idSelected: self.todoList[indexPath.row].id)
            
            self.todoList = self.todoRepository.getTodoItems(done: true)
            self.tableViewDone.reloadData()
            self.navigationController?.navigationBar.topItem?.title = "Feitas (" + String(self.todoList.count) + ")"
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel))
        
        self.present(alert, animated: true)
    }
    
    @objc func newTodo() {
        showInputDialog(title: "Nova Tarefa", subtitle: "Qual o vai ser o íncrivel nome da sua tarefa?") {
            (input:String?) in
                self.todoRepository.AddItem(todo: input ?? "")
                self.todoList = self.todoRepository.getTodoItems(done: true)
                self.tableViewDone.reloadData()
                self.navigationController?.navigationBar.topItem?.title = "Feitas (" + String(self.todoList.count) + ")"
        }
    }
}
