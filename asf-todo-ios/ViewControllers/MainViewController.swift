import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    var todoList:[Todo] = []
    let todoRepository = TodoRepository()

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.todoList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId") as! TodoTableViewCell
        cell.labelInclusion?.text = cell.dateFormatToString(date: todoList[indexPath.row].inclusion)
        cell.labelDescription?.text = todoList[indexPath.row].description
        cell.imgStatus?.image = UIImage(named: todoList[indexPath.row].isdone ? "item-selected" : "item-unselected")
        return cell;
    }
    
    
    
    
    func showInputDialog(title:String? = nil, subtitle:String? = nil, actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        
        alert.addTextField { (textField:UITextField) in
            textField.keyboardType = UIKeyboardType.default
        }
        
        alert.addAction(UIAlertAction(title: "Adicionar", style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            
            actionHandler?(textField.text)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
